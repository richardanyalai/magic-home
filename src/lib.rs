//! # Magic Home
//!
//! `magic_home` is a simple unofficial implementation of the Magic Home Wifi
//! smart device API in Rust. You can control compatible led stripes and
//! lightbulbs using this API easily. Follow the instructions in the README.md
//! file in the root of the repository to learn how to use the API.

use std::{
	io::prelude::*,
	net::{IpAddr, Shutdown, SocketAddr, TcpStream},
	time::{Duration, SystemTime as Time},
};

/// Port number used by the MagicHome API
const API_PORT: u16 = 5577;

/// Struct representing a Magic Home Wifi smart device
///
/// - `device_type` - the type of the device (1 - 4)
/// - `ip_addr`- IP address of the device
/// - `keep_alive` - whether the connection should be kept alive after timeout
/// - `last_conn` - timestamp of the last connection
/// - `stream` - the TCP stream
#[rustfmt::skip]
pub struct MagicHomeDevice {
	device_type: u8,
	ip_addr    : String,
	keep_alive : bool,
	last_conn  : Time,
	stream     : TcpStream,
}

/// Helper function for creating TCP streams
fn create_stream(ip_addr: &str) -> TcpStream {
	TcpStream::connect_timeout(
		&SocketAddr::from((
			ip_addr
				.parse::<IpAddr>()
				.unwrap_or_else(|_| panic!("Invalid IP address {}", ip_addr)),
			API_PORT,
		)),
		Duration::from_secs(3),
	)
	.expect("Couldn't create socket")
}

impl MagicHomeDevice {
	/// Creates a new instance of a Magic Home device
	///
	/// - `ip_addr`- IP address of the device
	/// - `device_type` - the type of the device (1 - 4)
	pub fn new(ip_addr: &str, device_type: u8) -> Self {
		MagicHomeDevice {
			device_type,
			ip_addr: ip_addr.into(),
			keep_alive: true,
			last_conn: Time::now(),
			stream: create_stream(ip_addr),
		}
	}

	/// Gets data from the device through TCP
	pub fn get_status(&mut self) -> Vec<u8> {
		let mut buf = vec![0u8; if self.device_type == 2 { 15 } else { 14 }];

		self.send_bytes(&[0x81, 0x8A, 0x8B, 0x96])
			.expect("Failure during write");

		self.stream
			.read_exact(&mut buf)
			.expect("Failure during read");

		buf
	}

	/// Sends an arra of bytes to the device through TCP
	///
	/// - bytes - array of bytes
	fn send_bytes(&mut self, bytes: &[u8]) -> std::io::Result<()> {
		let time_diff = Time::now().duration_since(self.last_conn).unwrap();

		// if the device should be kept alive, reconnect it when the connection
		// gets timed out
		if time_diff.as_secs() >= 3 {
			println!("Connection timed out, reconnecting...");
			self.stream = create_stream(&self.ip_addr);
			self.last_conn = Time::now();
		}

		self.stream.write_all(bytes)?;

		if !self.keep_alive {
			self.disconnect().expect("Failure during disconnection")
		}

		Ok(())
	}

	/// Updates device settings
	///
	/// - `r` - red channel
	/// - `g` - green channel
	/// - `b` - blue channel
	/// - `w1` - white 1 (if available)
	/// - `w2` - white 2 (if available)
	pub fn update_device(
		&mut self,
		r: u8,
		g: u8,
		b: u8,
		w1: Option<u8>,
		w2: Option<u8>,
	) -> std::io::Result<()> {
		#[rustfmt::skip]
		let mut bytes: Vec<u8> = match self.device_type {
			0 | 1 => vec![0x31, r, g, b, w1.unwrap(), 0x00, 0x0f],
			2 => vec![0x31, r, g, b, w1.unwrap(), w2.unwrap(), 0x0f, 0x0f],
			3 => {
				if let Some(w1) = w1 {
					vec![0x31, 0x00, 0x00, 0x00, w1, 0x0f, 0x0f]
				} else {
					vec![0x31, r, g, b, 0x00, 0xf0, 0x0f]
				}
			}
			4 => {
				if let Some(w1) = w1 {
					vec![
						0x56, 0x00, 0x00, 0x00, w1, 0x0f, 0xaa,
						0x56, 0x00, 0x00, 0x00, w1, 0x0f, 0xaa
					]
				} else {
					vec![0x56, r, g, b, 0x00, 0xf0, 0xaa]
				}
			}
			_ => panic!("Unsupported device"),
		};

		bytes.push(
			(bytes
				.iter()
				.map(|byte| *byte as u16)
				.reduce(|chksum, byte| (chksum + byte))
				.unwrap()) as u8,
		);

		self.send_bytes(&bytes)?;

		Ok(())
	}

	/// Turns on the device
	pub fn turn_on(&mut self) -> std::io::Result<()> {
		self.send_bytes(if self.device_type < 4 {
			&[0x71, 0x23, 0x0F, 0xA3]
		} else {
			&[0xCC, 0x23, 0x33]
		})?;

		Ok(())
	}

	/// Turns off the device
	pub fn turn_off(&mut self) -> std::io::Result<()> {
		self.send_bytes(if self.device_type < 4 {
			&[0x71, 0x24, 0x0F, 0xA4]
		} else {
			&[0xCC, 0x24, 0x33]
		})?;

		Ok(())
	}

	/// Closes the TCP stream
	pub fn disconnect(&self) -> std::io::Result<()> {
		// close both reading and writing
		self.stream.shutdown(Shutdown::Both)?;

		Ok(())
	}
}
