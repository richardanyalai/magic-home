# magic_home.rs

This is my version of the Magic Home Wifi smart device API implemented in Rust
according to the one that was written in Python by
[AdamKempenich](https://github.com/adamkempenich/magichome-python). It can be
used to control compatible led stripes and lightbulbs easily.

## Usage Examples

```rs
// Instantiating a device controller with the IP address and type of the device
let mut controller = MagicHomeDevice::new("192.168.88.11", 3);

// Calling methods
//
// All the methods return with std::io::Result so the return value should
// be handled by the user. In the following examples I will just ignore the
// result and the program will panic on any error.

// Turning on the lights
controller.turn_on().unwrap();

// Turning off the lights
controller.turn_off().unwrap();

// Update device settings
// Parameters:            R    G    B     W1       W2
controller.update_device(255, 255, 255, Some(69), None).unwrap();

// Get data from the device and print it to the standard output using the debug
// macro
dbg!(controller.get_status());

// Disconnecting from the device
controller.disconnect().unwrap();
```
